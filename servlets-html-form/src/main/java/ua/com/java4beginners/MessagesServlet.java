package ua.com.java4beginners;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(loadOnStartup = 1, urlPatterns = "/*")
public class MessagesServlet extends HttpServlet {

 @Override
 protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     String html = "<html>" +
             "<body>" +
             "  <form method='POST' action='/'>" +
             "    <label>Message</label>" +
             "    <input type='text' name='message'>" +
             "    <button type='submit'>Submit</button>" +
             "  </form>" +
             "</body></html";
     resp.getWriter().write(html);
 }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getParameter("message"));
        resp.sendRedirect("/");
    }

}
