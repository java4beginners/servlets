package ua.com.java4beginners;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(loadOnStartup = 1, urlPatterns = "/")
public class ProductListServlet extends HttpServlet {

  static Map<Long, Product> products = new HashMap<Long, Product>(){{
      put(1L, new Product(1L, "NoteBook", "Just a notebook.", 100, null));
      put(2L, new Product(2L, "Cup", "Just a cup.", 125, null));
  }};

 @Override
 protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     PrintWriter out = resp.getWriter();
     out.write("<html><body><h1>Products</h1><ul>");
     for (Product product : products.values()) {
         out.write(String.format("<li>%s, %d", product.getTitle(), product.getPrice()));
         String addToCartButtonHtml = "<form method='POST' action='/cart/products'>" +
                 "<input type='hidden' name='id' value='%d'>" +
                 "<button type='submit'>Add</button>" +
                 "</form>";
         out.write(String.format(addToCartButtonHtml, product.getId()));
     }
   out.write("</ul></body></html>");
 }



}
