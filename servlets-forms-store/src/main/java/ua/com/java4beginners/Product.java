package ua.com.java4beginners;

public class Product {
    private Long id;
    private String title;
    private String description;
    private Integer price;
    private String imageUrl;

    public Product(Long id, String title, String description, Integer price, String imageUrl) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
