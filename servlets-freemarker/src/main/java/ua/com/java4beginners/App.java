package ua.com.java4beginners;

import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Lists.newArrayList;
import static freemarker.template.Configuration.VERSION_2_3_21;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(loadOnStartup = 1, urlPatterns = "/*")
public class App extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    User user = new User("Ivan", new User("John", null));
    List<String> tasks = newArrayList("Get FreeMarker dependency.", "Move all html out from servlets.", "Be happy!");
    write(resp.getWriter(), of("user", user, "tasks", tasks)); // Map of objects that will be used in FTL code.
  }

  private void write(Writer out, Map data) throws IOException {
    try {
      getTemplate("index.ftl").process(data, out);
    } catch (TemplateException e) {
      throw new RuntimeException(e);
    }
  }

  private Template getTemplate(String name) throws IOException {
    Configuration config = new Configuration(VERSION_2_3_21);
    URL resource = getClass().getClassLoader().getResource(".");
    String path = resource.getPath();
    config.setDirectoryForTemplateLoading(new File(path));
    return config.getTemplate(name);
  }

  public static class User {
    private String name;
    private User friend;
    public User(String name, User friend) {
      this.name = name;
      this.friend = friend;
    }
    public String getName() { return name; }
    public User getFriend() { return friend; }
  }
}
