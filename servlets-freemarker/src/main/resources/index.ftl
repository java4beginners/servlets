<!DOCTYPE html>
<html>
<head>
  <title>Hello App</title>
</head>
<body>
<h1>Hello  ${user.name}, fried of ${user.friend.name}!</h1>
<h3>Tasks:</h3>
<#list tasks as task>
  <li>${task}</li>
</#list>
</body>
</html>
